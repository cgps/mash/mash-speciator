const fs = require('fs');
const path = require('path');

const referencesDir = path.join(__dirname, '..', '..', 'references');
const idsFile = path.join(referencesDir, 'missing-ids.csv');

function main() {
  const ids = fs.readFileSync(idsFile, 'utf8').split('\n').filter(x => x !== '');
  for (const id of ids) {
    const file = path.join(referencesDir, 'metadata', `${id}.stats.txt`);
    const stats = fs.readFileSync(file, 'utf8');
    const taxid = /# Taxid: {10}(.*)\r\n/.exec(stats)[1];
    const organismName = /# Organism name: {2}(.*)\r\n/.exec(stats)[1];
    console.log([ id, taxid, taxid, organismName ].join(','));
  }
}

main();
