/* eslint no-console: 0 */
/* eslint no-unused-vars: 0 */

const fs = require('fs');
const path = require('path');
const createMashSpecieator = require('../index');

const referencesDir = require('mash-sketches');
const referenceName = 'refseq-archaea-bacteria-fungi-viral-k16-s400';
const sketchFilePath = path.join(referencesDir, `${referenceName}.msh`);
const metadataFilePath = path.join(referencesDir, `${referenceName}.csv`);
const fastaFilePath = process.argv[2];

const specieator = createMashSpecieator(sketchFilePath, metadataFilePath, 'native');

function wirteResults(file, results) {
  const values = Object.keys(results).map(key => `${key}=${results[key]}`);
  values.unshift(file);
  console.log(
    values.join(',')
  );
}

function testOneFile() {
  return specieator.queryFile(fastaFilePath)
    .then(results => {
      wirteResults(fastaFilePath, results);
    })
    .catch(error => {
      console.error(error);
    });
}

testOneFile();
