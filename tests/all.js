/* eslint no-console: 0 */
/* eslint no-unused-vars: 0 */

const fs = require('fs');
const path = require('path');
const createMashSpecieator = require('../index');

const referencesDir = path.join(__dirname, '..', 'references');
const referenceName = 'refseq-archaea-bacteria-fungi-viral-k16-s400';
const sketchFilePath = path.join(referencesDir, `${referenceName}.msh`);
const metadataFilePath = path.join(referencesDir, `${referenceName}.csv`);
const fastaFilePath = path.join(referencesDir, 'MRSA01.fa');

const mashWrapper = require('mash-node-wrapper');
const mashNative = require('mash-node-native');

const specieator
  = createMashSpecieator(sketchFilePath, metadataFilePath);

const processFile = specieator.queryFile;

// function testOneFile() {
//   return specieator.queryFile(fastaFilePath)
//     .then(results => {
//       console.log(results);
//     })
//     .catch(error => {
//       console.error(error);
//     });
// }

function testFolder() {
  const folder = process.argv[2];
  const promises =
    fs.readdirSync(folder)
      .filter(file => /\.fa(sta)?$/i.test(file))
      .map(file => path.join(folder, file))
      .map(file => specieator.queryFile(file).then(res => console.log(res) || res));
  // Promise.all(promises)
  //   .then(results =>
  //     console.log(
  //       results.filter((item) => item.taxId !== '90370' && item.speciesTaxId !== '90370')
  //     )
  //   );
}





function testQueue() {
  const folder = process.argv[2];
  const queue =
    fs.readdirSync(folder)
      .filter((file, i) => i < 1000 && /\.fa(sta)?$/i.test(file))
      .map((file) => path.join(folder, file));

  console.log('Queued files: ', queue.length);

  const popQueue = () => {
    if (queue.length > 0) {
      const file = queue.pop();
      console.log(file);
      return processFile(file)
        .then(results => console.log(file, results))
        .then(popQueue)
        .catch(err => console.error(err));
    }
    return new Promise((resolve, reject) => {
      console.log('Queue is empty.');
    });
  };

  return popQueue();
}

// function testContent() {
//   const fileContents = fs.readFileSync(fastaFilePath, 'utf8');
//   specieator.queryContents(fileContents)
//     .then(results => {
//       console.log(results);
//     })
//     .catch(error => {
//       console.error(error);
//     });
// }

// testOneFile()
//   .then(() => {
//     console.log('Starting in 10 seconds...');
//     setTimeout(() => {
//       testQueue();
//     }, 10 * 1000);
//   });

// console.log('Starting in 10 seconds...');
// setTimeout(() => {
//   testQueue();
// }, 10 * 1000);

var http = require('http');
http.createServer(function (req, res) {
  res.writeHead(200, {'Content-Type': 'text/plain'});
  res.end('Hello World\n');
}).listen(8124, "127.0.0.1");

testQueue();
