/* eslint no-console: 0 */
/* eslint no-unused-vars: 0 */

const fs = require('fs');
const path = require('path');
const createMashSpecieator = require('../index');

const referencesDir = require('mash-sketches');
const referenceName = 'refseq-archaea-bacteria-fungi-viral-k16-s400';
const sketchFilePath = path.join(referencesDir, `${referenceName}.msh`);
const metadataFilePath = path.join(referencesDir, `${referenceName}.csv`);
const fastaFilePath = process.argv[2];

const specieator
  = createMashSpecieator(sketchFilePath, metadataFilePath);

function testOneFile() {
  return specieator.queryFile(fastaFilePath)
    .then(results => {
      console.log(results);
    })
    .catch(error => {
      console.error(error);
    });
}

testOneFile();
