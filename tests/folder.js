/* eslint no-console: 0 */
/* eslint no-unused-vars: 0 */

const fs = require('fs');
const path = require('path');
const createMashSpecieator = require('../index');

const referencesDir = require('mash-sketches');
const referenceName = 'refseq-archaea-bacteria-fungi-viral-k16-s400';
const sketchFilePath = path.join(referencesDir, `${referenceName}.msh`);
const metadataFilePath = path.join(referencesDir, `${referenceName}.csv`);
const fastaFilePath = path.join(referencesDir, 'MRSA01.fa');

// const mashWrapper = require('mash-node-wrapper');
// const mashNative = require('mash-node-native');

const specieator
  = createMashSpecieator(sketchFilePath, metadataFilePath);

const folder = process.argv[2];
fs.readdirSync(folder)
  .filter(subfolder => !/^\./i.test(subfolder))
  .map(subfolder =>
    fs.readdirSync(path.join(folder, subfolder))
      .filter(file => /\.fa(sta)?$/i.test(file))
      .map(
        file =>
          specieator
            .queryFile(path.join(folder, subfolder, file))
            .then((result) => console.log(file, subfolder, result))
      )
  );
